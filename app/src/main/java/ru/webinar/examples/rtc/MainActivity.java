package ru.webinar.examples.rtc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import org.webrtc.BuiltinAudioDecoderFactoryFactory;
import org.webrtc.BuiltinAudioEncoderFactoryFactory;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.DataChannel;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.RtpTransceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static final AtomicBoolean initialized = new AtomicBoolean();

    private SurfaceViewRenderer localViewRenderer;
    private SurfaceViewRenderer remoteViewRenderer;

    PeerConnection remotePeer;
    PeerConnection localPeer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (initialized.compareAndSet(false, true)) {
            PeerConnectionFactory.initialize(PeerConnectionFactory.InitializationOptions.builder(getApplicationContext()).createInitializationOptions());
        }

        EglBase.Context ctx = EglBase.create().getEglBaseContext();
        PeerConnectionFactory factory = PeerConnectionFactory.builder()
                .setVideoEncoderFactory(new DefaultVideoEncoderFactory(ctx, true, true))
                .setAudioEncoderFactoryFactory(new BuiltinAudioEncoderFactoryFactory())
                .setVideoDecoderFactory(new DefaultVideoDecoderFactory(ctx))
                .setAudioDecoderFactoryFactory(new BuiltinAudioDecoderFactoryFactory())
                .createPeerConnectionFactory();

        localViewRenderer = findViewById(R.id.localViewRenderer);
        localViewRenderer.init(ctx, null);

        remoteViewRenderer = findViewById(R.id.remoteViewRenderer);
        remoteViewRenderer.init(ctx, null);

        PeerConnection.RTCConfiguration config = new PeerConnection.RTCConfiguration(new ArrayList<PeerConnection.IceServer>());

        localPeer = factory.createPeerConnection(config, new AbstractPeerConnectionObserver() {
            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                remotePeer.addIceCandidate(iceCandidate);
            }
        });
        remotePeer = factory.createPeerConnection(config, new AbstractPeerConnectionObserver() {
            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                localPeer.addIceCandidate(iceCandidate);
            }

            @Override
            public void onAddStream(MediaStream mediaStream) {
                super.onAddStream(mediaStream);
                List<VideoTrack> video = mediaStream.videoTracks;
                if (video != null && video.size() > 0) {

                    remoteViewRenderer.post(() -> {
                        VideoTrack track = video.get(0);
                        track.addSink(remoteViewRenderer);
                        track.setEnabled(true);
                    });
                }
            }
        });

        localViewRenderer.post(() -> {
            VideoSource source = factory.createVideoSource(false);

            CameraEnumerator enumerator;
            if (Camera2Enumerator.isSupported(this)) {
                enumerator = new Camera2Enumerator(this);
            } else {
                enumerator = new Camera1Enumerator();
            }
            for (String name : enumerator.getDeviceNames()) {
                if (!enumerator.isFrontFacing(name)) {
                    continue;
                }
                CameraVideoCapturer capturer = enumerator.createCapturer(name, null);
                capturer.initialize(
                        SurfaceTextureHelper.create("capture", ctx),
                        getApplicationContext(),
                        source.getCapturerObserver());

                VideoTrack track = factory.createVideoTrack("video", source);
                track.addSink(localViewRenderer);
                track.setEnabled(true);

                localPeer.addTrack(track);
                capturer.startCapture(320, 240, 30);
            }

            localPeer.createOffer(new AbstractSdpObserver() {
                private SessionDescription offer;
                private SessionDescription answer;

                @Override
                public void onCreateSuccess(SessionDescription sessionDescription) {
                    offer = new SessionDescription(sessionDescription.type, disableVpx(sessionDescription.description));
                    System.out.printf("offer: %s", offer.description);
                    localPeer.setLocalDescription(this, offer);
                }

                @Override
                public void onSetSuccess() {
                    remotePeer.setRemoteDescription(new AbstractSdpObserver() {
                        @Override
                        public void onSetSuccess() {
                            MediaConstraints constraints = new MediaConstraints();
                            constraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"));
                            constraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
                            remotePeer.createAnswer(this, constraints);
                        }

                        @Override
                        public void onCreateSuccess(SessionDescription sessionDescription) {
                            answer = sessionDescription;
                            System.out.printf("answer: %s", answer.description);
                            remotePeer.setLocalDescription(new AbstractSdpObserver() {
                                @Override
                                public void onSetSuccess() {
                                    localPeer.setRemoteDescription(new AbstractSdpObserver(), answer);
                                }
                            }, answer);
                        }
                    }, offer);
                }
            }, new MediaConstraints());
        });
    }

    private static String disableVpx(String sdp) {
        Matcher m = Pattern.compile("^a=rtpmap:(\\d+) VP(8|9)/90000$", Pattern.MULTILINE).matcher(sdp);
        Set<String> payload = new HashSet<>();
        while (m.find()) {
            payload.add(m.group(1));
        }
        payload.add("97");
        payload.add("99");
        for (String p : payload) {
            sdp = sdp.replaceAll(String.format("a=fmtp:%s.*\r\n", p), "");
            sdp = sdp.replaceAll(String.format("a=rtpmap:%s.*\r\n", p), "");
            sdp = sdp.replaceAll(String.format("a=rtcp-fb:%s.*\r\n", p), "");
            sdp = sdp.replaceAll(String.format(" %s\\b", p), "");
        }
        return sdp;
    }

    private class AbstractPeerConnectionObserver implements PeerConnection.Observer {

        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
        }

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        }

        @Override
        public void onStandardizedIceConnectionChange(PeerConnection.IceConnectionState newState) {
        }

        @Override
        public void onConnectionChange(PeerConnection.PeerConnectionState newState) {
        }

        @Override
        public void onIceConnectionReceivingChange(boolean b) {
        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        }

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {
        }

        @Override
        public void onRenegotiationNeeded() {
        }

        @Override
        public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
        }

        @Override
        public void onTrack(RtpTransceiver transceiver) {
        }
    }

    private class AbstractSdpObserver implements SdpObserver {

        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
        }

        @Override
        public void onSetSuccess() {
        }

        @Override
        public void onCreateFailure(String s) {
            System.out.println(s);
        }

        @Override
        public void onSetFailure(String s) {
            System.out.println(s);
        }
    }
}